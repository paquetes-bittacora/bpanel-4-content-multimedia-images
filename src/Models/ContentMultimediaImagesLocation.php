<?php

namespace Bittacora\ContentMultimediaImages\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentMultimediaImagesLocation extends Model
{
    use HasFactory;

    protected $table = 'content_multimedia_images_location';
    public $timestamps = false;

    public $fillable = [
        'module', 'name'
    ];
}
