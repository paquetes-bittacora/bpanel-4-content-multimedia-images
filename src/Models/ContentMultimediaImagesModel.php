<?php

namespace Bittacora\ContentMultimediaImages\Models;

use Bittacora\Content\Models\ContentModel;
use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class ContentMultimediaImagesModel extends Model implements Sortable
{
    use HasFactory, SortableTrait;


    protected $table = 'content_multimedia_images';
    public $timestamps = false;
    public $incrementing = true;

    public $fillable = [
        'content_id',
        'multimedia_id',
        'location',
        'active',
        'featured'
    ];


    public $sortable = [
        'sort_when_creating' => true
    ];

    public function content(){
        return $this->belongsTo(ContentModel::class, 'content_id', 'id');
    }

    public function multimedia(){
        return $this->hasOne(Multimedia::class, 'id','multimedia_id');
    }

    public function buildSortQuery(){
        return static::query()->where([
            ['content_id', '=', $this->content_id]
        ]);
    }

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
    }
}
