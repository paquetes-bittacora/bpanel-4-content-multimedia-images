<?php

namespace Bittacora\ContentMultimediaImages;

use Bittacora\ContentMultimediaImages\Http\Livewire\ContentMultimediaImagesLocationSelect;
use Bittacora\ContentMultimediaImages\Http\Livewire\ContentMultimediaImagesWidget;
use Bittacora\ContentMultimediaImages\Http\Livewire\ContentMultimediaImagesWidgetTable;
use Bittacora\ContentMultimediaImages\Http\Livewire\DetachImagesMultimediaButton;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\ContentMultimediaImages\Commands\ContentMultimediaImagesCommand;

class ContentMultimediaImagesServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('content-multimedia-images')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_content-multimedia-images_table')
            ->hasCommand(ContentMultimediaImagesCommand::class);
    }

    public function register()
    {
        $this->app->bind('content-multimedia-images', function($app){
            return new ContentMultimediaImages();
        });

        $this->app->bind('content-multimedia-images-location', function($app){
            return new ContentMultimediaImagesLocation();
        });
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        Livewire::component('content-multimedia-images::content-multimedia-images-widget', ContentMultimediaImagesWidget::class);
        Livewire::component('content-multimedia-images::content-multimedia-images-location-select', ContentMultimediaImagesLocationSelect::class);
        Livewire::component('content-multimedia-images::content-multimedia-images-widget-table', ContentMultimediaImagesWidgetTable::class);

        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'content-multimedia-images');
    }
}
