<?php

namespace Bittacora\ContentMultimediaImages;

use Illuminate\Support\Facades\Cache;

class ContentMultimediaImagesLocation
{
    public function createNewLocation(string $module, string $name){
        Models\ContentMultimediaImagesLocation::create([
            'module' => $module,
            'name' => $name
        ]);
    }

    public function getModuleLocations(string $module){
        if(!Cache::has($module."ImagesLocation")){
            $imagesLocation = Models\ContentMultimediaImagesLocation::where('module', $module)->get();
            Cache::add($module.'ImagesLocation', $imagesLocation);
        }else{
            $imagesLocation = Cache::get($module.'ImagesLocation');
        }
        return $imagesLocation;
    }
}
