<?php

namespace Bittacora\ContentMultimediaImages\Commands;

use Illuminate\Console\Command;

class ContentMultimediaImagesCommand extends Command
{
    public $signature = 'content-multimedia-images';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
