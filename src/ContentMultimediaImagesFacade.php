<?php

namespace Bittacora\ContentMultimediaImages;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\ContentMultimediaImages\ContentMultimediaImages
 */
class ContentMultimediaImagesFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'content-multimedia-images';
    }
}
