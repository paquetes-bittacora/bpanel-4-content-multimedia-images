<?php

namespace Bittacora\ContentMultimediaImages;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\ContentMultimedia\ContentMultimediaLocation
 */
class ContentMultimediaImagesLocationFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'content-multimedia-images-location';
    }
}
