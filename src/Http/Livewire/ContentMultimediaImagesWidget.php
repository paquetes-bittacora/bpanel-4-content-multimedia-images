<?php

namespace Bittacora\ContentMultimediaImages\Http\Livewire;

use Bittacora\ContentMultimediaImages\Models\ContentMultimediaImagesModel;
use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;
use Symfony\Component\VarDumper\VarDumper;

class ContentMultimediaImagesWidget extends Component
{

    public int $contentId;
    public string $module;
    public ?Collection $images = null;

    protected $listeners = ['refreshWidget' => '$refresh'];

    public function mount()
    {
        $contentMultimediaData = ContentMultimediaImagesModel::where([
            ['content_id', '=', $this->contentId]
        ])->orderBy('order_column', 'ASC')->get();

        $this->images = $contentMultimediaData;

//        if (!empty($contentMultimediaData)){
//            foreach ($contentMultimediaData as $key => $content) {
//                /**
//                 * @var ContentMultimediaImagesModel $content
//                 */
//                $multimedia = Multimedia::where('id', $content->multimedia_id);
//                $content->setAttribute('multimedia', $multimedia);
//
//            }
//
//            $this->images = $contentMultimediaData;
//        }
    }

    public function render()
    {
        return view('content-multimedia-images::livewire.content-multimedia-images-widget')->with([
            'images' => $this->images,
            'contentId' => $this->contentId
        ]);
    }
}
