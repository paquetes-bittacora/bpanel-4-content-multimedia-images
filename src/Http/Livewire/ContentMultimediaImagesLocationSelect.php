<?php

namespace Bittacora\ContentMultimediaImages\Http\Livewire;

use Bittacora\ContentMultimediaImages\ContentMultimediaImagesLocationFacade;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class ContentMultimediaImagesLocationSelect extends Component
{
    public string $module;
    public Collection $options;
    public Model $model;
    public $selectedValue = null;

    protected $listeners = ['refreshContentMultimediaLocationSelect' => '$refresh'];

    public function mount(){
        $this->options = ContentMultimediaImagesLocationFacade::getModuleLocations($this->module);
        $this->selectedValue = $this->model->location;
    }
    public function render()
    {
        return view('content-multimedia-images::livewire.content-multimedia-images-location-select')->with([
            'options' => $this->options,
            'model' => $this->model
        ]);
    }

    public function changeLocation(){
        if($this->selectedValue != ''){
            $result = $this->model->update(['location' => $this->selectedValue]);
        }else{
            $result = $this->model->update(['location' => null]);
        }

        if(!$result){
            session()->flash('message', ['text' => 'Error al actualizar.', 'type' => 'danger', 'icon' => 'fa fa-times-circle']);
        }else{
            session()->flash('message', ['text' => 'Posición actualizada.', 'type' => 'success', 'icon' => 'fa fa-check-circle']);
        }
    }

}
