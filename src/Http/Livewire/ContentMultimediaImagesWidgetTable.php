<?php

namespace Bittacora\ContentMultimediaImages\Http\Livewire;

use Bittacora\ContentMultimediaImages\Models\ContentMultimediaImagesModel;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ContentMultimediaImagesWidgetTable extends DataTableComponent
{

    public int $contentId;
    public bool $reordering = true;
    public string $reorderingMethod = 'reorder';
    public bool $reorderEnabled = false;
    public bool $showPerPage = false;
    public bool $showPagination = false;
    public bool $showSearch = false;
    public array $perPageAccepted = [100];
    public bool $selectAll = false;
    public bool $perPageAll = false;
    public bool $paginationEnabled = false;

    public ?string $module;

    protected $listeners = ['refreshContentMultimediaImagesWidgetTable' => '$refresh'];

    public function columns(): array
    {
        return [
            Column::make('Previsualización')->addClass('text-center w-10'),
            Column::make('Título')->addClass('w-30'),
            Column::make('Posición')->addClass('w-20'),
            Column::make('Activo')->addClass('w-10 text-center'),
            Column::make('Destacado')->addClass('w-10 text-center'),
            Column::make('Orden', 'order_column')->addClass('w-10 text-center'),
            Column::make('')->addClass('w-10')
        ];
    }

    public function query()
    {
        return ContentMultimediaImagesModel::query()->where('content_id', $this->contentId)->orderBy('order_column', 'ASC');
    }

    public function rowView(): string
    {
        return 'content-multimedia-images::livewire.content-multimedia-images-widget-table';
    }


    public function reorder($list){
        foreach($list as $item){
            ContentMultimediaImagesModel::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }
}
