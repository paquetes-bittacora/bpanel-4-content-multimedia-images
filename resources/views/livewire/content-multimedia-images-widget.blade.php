<div class="widget-box widget-color-blue ui-sortable-handle mb-4" id="widget-box-7">
    <div class="widget-header widget-header-small">
        <h6 class="widget-title smaller">Imágenes adjuntas</h6>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            @livewire('content-multimedia-images::content-multimedia-images-widget-table', ['contentId' => $contentId, 'module' => $module], key($contentId))
            @livewire('multimedia::media-library-images', ['contentId' => $contentId, 'type' => 'images'], key('media-library-images-'.$contentId))
{{--            @livewire('media-library-table')--}}
        </div>
    </div>
</div>
@push('scripts')
    @vite('vendor/bittacora/bpanel4-panel/resources/assets/js/livewire-sortable.js')
@endpush
