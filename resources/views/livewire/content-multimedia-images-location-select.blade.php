<div class="position-relative">
    <select class="form-control content-multimedia-location-select" id="{{$model->id}}" wire:model="selectedValue" wire:change="changeLocation()">
        <option value="">-- Sin asignar --</option>
        @foreach($options as $option)
            <option value="{{$option->id}}">{{$option->name}}</option>
        @endforeach
    </select>
    <div class="w-100" wire:loading>
        <div class="bg-info text-white py-1 px-2 mt-1">
            <i class="fa fa-info-circle"></i> Modificando posición...
        </div>
    </div>

    @if (session()->has('message'))
        <div class="box-message-{{session()->get('message')['type']}} bg-{{session()->get('message')['type']}} text-white py-1 px-2 mt-1">
            <i class="{{session()->get('message')['icon']}}"></i> {{session()->get('message')['text']}}
        </div>
    @endif
</div>

@push('scripts')
    <script>
        setInterval(function(){
            $(".box-message-success").fadeOut(1000);
            $(".box-message-danger").fadeOut(1000);
        }, 1000);
    </script>
@endpush
