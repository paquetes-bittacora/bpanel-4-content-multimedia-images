<td>
    @livewire('utils::media-preview', ['media' => $row->multimedia->mediaModel], key('image-images-'.$row->id))
</td>
<td>
    @if(!empty($row->multimedia->title))
        {{$row->multimedia->title}}
    @else
        {{$row->multimedia->mediaModel->file_name}}
    @endif
</td>
<td>
    @livewire('content-multimedia-images::content-multimedia-images-location-select', ['model' => $row, 'module' => $module], key('location-images-'.$row->id))
</td>
<td>
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-images-'.$row->id))
</td>
<td>
    @livewire('utils::datatable-default', ['fieldName' => 'featured', 'model' => $row, 'value' => $row->featured, 'size' => 'xxs'], key('featured-images-'.$row->id))
</td>
<td>
    <div class="d-flex justify-content-center align-items-center">
        <span class="align-items-center badge bgc-purple-d1 pos-rel text-white radius-4 px-3">
            <span class="bgc-primary-tp4 opacity-5 position-tl h-100 w-100 radius-4"></span>
            <span class="pos-rel">
                {{$row->order_column}}
            </span>
        </span>
    </div>
</td>
<td>
    @livewire('content-multimedia::detach-multimedia-button', ['contentMultimediaId' => $row->id, 'contentId' => $row->content_id, 'type' => 'I'], key('detach-images-'.$row->multimedia_id))
</td>
